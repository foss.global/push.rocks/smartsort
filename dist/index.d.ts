/// <reference types="lodash" />
export declare let sortBy: {
    <T, TSort>(collection: _.List<T>, iteratee?: _.ListIterator<T, TSort>): T[];
    <T, TSort>(collection: _.Dictionary<T>, iteratee?: _.DictionaryIterator<T, TSort>): T[];
    <T>(collection: _.List<T> | _.Dictionary<T>, iteratee: string): T[];
    <W extends {}, T>(collection: _.List<T> | _.Dictionary<T>, whereValue: W): T[];
    <T>(collection: _.List<T> | _.Dictionary<T>): T[];
    <T>(collection: _.List<T>, iteratees: (string | Object | _.ListIterator<T, any>)[]): T[];
    <T>(collection: _.List<T>, ...iteratees: (string | Object | _.ListIterator<T, boolean>)[]): T[];
};
